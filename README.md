
#Example Usage

```
---

- hosts: all
  gather_facts: false
  vars_prompt:
    - name: "service_name"
      prompt: "service name"
      private: no
    - name: "cluster_host"
      prompt: "cluster host"
      private: no
      default: 'global'
  vars_files: 
    - vars/{{service_name}}.yml
  roles:
    - role:  reactiveops.ansible-coreos-unit-files
      become: yes
```

#Example vars file

```
---

units:
  - description: 'Reactr'
    docker:
      image: 'nginx:latest'
      options: 
        - option: '--env-file'
          value: '/srv/data/reactr/.env'
        - option: '-P'
    service_opts:
      - option: 'Restart'
        value: 'always'
      - option: 'RestartSec'
        value: '20s'
      - option: 'TimeoutStartSec'
        value: '120s'
    #xfleet_opts:
      # - option: 'Conflicts'
      #   value: '%p@*.service'
```